package com.istevesitupdated;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.istevesit1.R;

public class Register extends Fragment{
    private static final String TAG = "Http Connection";

    //RegEvent_Adapter adapter;
    //private ProgressBar progressBar;

    //private ListView listView;

    //private ArrayAdapter arrayAdapter = null;

    //private String[] blogTitles;


    public Register() {
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register, container, false);


        final Button b1= (Button) v.findViewById ( R.id.registerbutton );
        final TextView title = (TextView) v.findViewById ( R.id.title1 );
        title.setText ( getArguments ().getString ( "event" ) );
        final TextView teamsize = (TextView) v.findViewById ( R.id.teamsize );
        teamsize.setText ( "Required Team Size : "+getArguments ().getInt ( "team" )  );

        final CardView cv2 = (CardView) v.findViewById ( R.id.teamcard2 );
        final CardView cv3 = (CardView) v.findViewById ( R.id.teamcard3 );

        if(getArguments ().getInt ( "team" ) == 3){
            cv2.setVisibility ( View.VISIBLE );
            cv3.setVisibility ( View.VISIBLE );
        }else if(getArguments ().getInt ( "team" ) == 2){
            cv2.setVisibility ( View.VISIBLE );
        }

        final EditText name1 = (EditText) v.findViewById ( R.id.member11 );
        final EditText clas1 = (EditText) v.findViewById ( R.id.member12 );
        final EditText number1 = (EditText) v.findViewById ( R.id.member13 );
        final EditText name2 = (EditText) v.findViewById ( R.id.member21 );
        final EditText clas2 = (EditText) v.findViewById ( R.id.member22 );
        final EditText number2 = (EditText) v.findViewById ( R.id.member23 );
        final EditText name3 = (EditText) v.findViewById ( R.id.member31 );
        final EditText clas3 = (EditText) v.findViewById ( R.id.member32 );
        final EditText number3 = (EditText) v.findViewById ( R.id.member33 );





        b1.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if(getArguments ().getInt ( "team" ) == 1) {
                    if(name1.getText ().toString ().length() == 0)                                                              //Name Validation
                {
                    name1.setError("Invalid Name.");
                    name1.requestFocus();
                    return;
                }

                if(clas1.getText ().toString ().length() == 0)                                                         //Class Name Validation
                {
                    clas1.setError("Invalid Class name.");
                    clas1.requestFocus();
                    return;
                }

                final String mobilePattern = "[0-9]{10}";                                           //Phone Number Validation

                if(!number1.getText ().toString ().matches(mobilePattern))
                {
                    number1.setError("Enter a valid 10-digit Mobile Number.");
                    number2.requestFocus();
                    return;
                }
                }
                else if(getArguments ().getInt ( "team" ) == 2){
                    if(name1.getText ().toString ().length() == 0)                                                              //Name Validation
                    {
                        name1.setError("Invalid Name.");
                        name1.requestFocus();
                        return;
                    }

                    if(clas1.getText ().toString ().length() == 0)                                                         //Class Name Validation
                    {
                        clas1.setError("Invalid Class name.");
                        clas1.requestFocus();
                        return;
                    }

                    final String mobilePattern = "[0-9]{10}";                                           //Phone Number Validation

                    if(!number1.getText ().toString ().matches(mobilePattern))
                    {
                        number1.setError("Enter a valid 10-digit Mobile Number.");
                        number1.requestFocus();
                        return;
                    }
                        if(name2.getText ().toString ().length() == 0)                                                              //Name Validation
                        {
                            name2.setError("Invalid Name.");
                            name2.requestFocus();
                            return;
                        }

                        if(clas2.getText ().toString ().length() == 0)                                                         //Class Name Validation
                        {
                            clas2.setError("Invalid Class name.");
                            clas2.requestFocus();
                            return;
                        }

                        //Phone Number Validation

                        if(!number2.getText ().toString ().matches(mobilePattern))
                        {
                            number2.setError("Enter a valid 10-digit Mobile Number.");
                            number2.requestFocus();
                            return;
                        }

                }
                else if(getArguments ().getInt ( "team" ) == 3){
                    if(name1.getText ().toString ().length() == 0)                                                              //Name Validation
                    {
                        name1.setError("Invalid Name.");
                        name1.requestFocus();
                        return;
                    }

                    if(clas1.getText ().toString ().length() == 0)                                                         //Class Name Validation
                    {
                        clas1.setError("Invalid Class name.");
                        clas1.requestFocus();
                        return;
                    }

                    final String mobilePattern = "[0-9]{10}";                                           //Phone Number Validation

                    if(!number1.getText ().toString ().matches(mobilePattern))
                    {
                        number1.setError("Enter a valid 10-digit Mobile Number.");
                        number1.requestFocus();
                        return;
                    }

                        if(name2.getText ().toString ().length() == 0)                                                              //Name Validation
                        {
                            name2.setError("Invalid Name.");
                            name2.requestFocus();
                            return;
                        }

                        if(clas2.getText ().toString ().length() == 0)                                                         //Class Name Validation
                        {
                            clas2.setError("Invalid Class name.");
                            clas2.requestFocus();
                            return;
                        }

                        //Phone Number Validation

                        if(!number2.getText ().toString ().matches(mobilePattern))
                        {
                            number2.setError("Enter a valid 10-digit Mobile Number.");
                            number2.requestFocus();
                            return;
                        }

                    if(name3.getText ().toString ().length() == 0)                                                              //Name Validation
                    {
                        name3.setError("Invalid Name.");
                        name3.requestFocus();
                        return;
                    }

                    if(clas3.getText ().toString ().length() == 0)                                                         //Class Name Validation
                    {
                        clas3.setError("Invalid Class name.");
                        clas3.requestFocus();
                        return;
                    }

                    //Phone Number Validation

                    if(!number3.getText ().toString ().matches(mobilePattern))
                    {
                        number3.setError("Enter a valid 10-digit Mobile Number.");
                        number3.requestFocus();
                        return;
                    }
                }
                RequestQueue queue = Volley.newRequestQueue(getContext ());
                String url="";
                if(getArguments ().getInt ( "team" ) == 1) {
                    url = "http://istevesit.org/json/register.php?id=" + getArguments ().getString ( "eventid" )
                            + "&name1=" + name1.getText () + "&class1=" + clas1.getText () + "&phone1=" + number1.getText ();
                }
                else if(getArguments ().getInt ( "team" ) == 2){
                    url = "http://istevesit.org/json/register.php?id=" + getArguments ().getString ( "eventid" )
                            + "&name1=" + name1.getText () + "&class1=" + clas1.getText () + "&phone1=" + number1.getText ()
                            + "&name2=" + name2.getText () + "&class2=" + clas2.getText () + "&phone2=" + number2.getText ();
                }
                else if(getArguments ().getInt ( "team" ) == 3){
                    url = "http://istevesit.org/json/register.php?id=" + getArguments ().getString ( "eventid" )
                            + "&name1=" + name1.getText () + "&class1=" + clas1.getText () + "&phone1=" + number1.getText ()
                            + "&name2=" + name2.getText () + "&class2=" + clas2.getText () + "&phone2=" + number2.getText ()
                            + "&name3=" + name3.getText () + "&class3=" + clas3.getText () + "&phone3=" + number3.getText ();
                }
                else{
                    url = "http://www.google.com";
                }
                StringRequest stringRequest = new StringRequest( Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Display the first 500 characters of the response string.
                                Toast.makeText ( getContext (),"Successfully Registered\n"+response.toString (),Toast.LENGTH_SHORT ).show ();
                                FragmentTransaction fragmentTransaction;
                                FragmentManager fragmentManager;
                                fragmentManager = getFragmentManager();
                                registration_main Register = new registration_main ();
                                fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                                fragmentTransaction.replace(R.id.fragmentholder,Register);
                                fragmentTransaction.commit();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText ( getContext (),"Error Registering",Toast.LENGTH_SHORT ).show ();
                    }
                });
                queue.add(stringRequest);
            }
        } );

//        final EditText etName = (EditText) v.findViewById(R.id.etName);
//        final EditText etClassName = (EditText) v.findViewById(R.id.etClassName);
//        final EditText etPhoneNumber = (EditText) v.findViewById(R.id.etPhoneNumber);
//        final Button btnRegister = (Button) v.findViewById(R.id.btnRegister);

//        etClassName.setText ( getArguments ().getString ( "event" ) );
//
//
//        btnRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final String name = etName.getText().toString();
//                final String className = etClassName.getText().toString();
//                final String phoneNumber = etPhoneNumber.getText().toString();
//
//                if(name.length() == 0)                                                              //Name Validation
//                {
//                    etName.setError("Invalid Name.");
//                    etName.requestFocus();
//                    return;
//                }
//
//                if(className.length() == 0)                                                         //Class Name Validation
//                {
//                    etClassName.setError("Invalid Class name.");
//                    etClassName.requestFocus();
//                    return;
//                }
//
//                final String mobilePattern = "[0-9]{10}";                                           //Phone Number Validation
//
//                if(!phoneNumber.matches(mobilePattern))
//                {
//                    etPhoneNumber.setError("Enter a valid 10-digit Mobile Number.");
//                    etPhoneNumber.requestFocus();
//                    return;
//                }
//
//                Response.Listener<String> responseListener = new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject jsonResponse = new JSONObject(response);
//                            boolean success = jsonResponse.getBoolean("success");
//                            if (success) {
//                                Fragment fragment = new Register();
//                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                                fragmentTransaction.replace(R.id.register, fragment);
//                                fragmentTransaction.addToBackStack(null);
//                                fragmentTransaction.commit();
//                                btnRegister.setVisibility(View.INVISIBLE);
//                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                                builder.setMessage("\tRegistration Successful!")
//                                        .setPositiveButton("All Set",null)
//                                        .create()
//                                        .show();
//                            } else {
//                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                                builder.setMessage("Registration Failed")
//                                        .setNegativeButton("Retry", null)
//                                        .create()
//                                        .show();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                };

//                RegisterRequest registerRequest = new RegisterRequest(name,className,phoneNumber, responseListener);
//                RequestQueue queue = Volley.newRequestQueue(getActivity());
//                queue.add(registerRequest);
//
//            }
//        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume ();
        getActivity().setTitle("Register");
    }
}
