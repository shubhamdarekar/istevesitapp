package com.istevesitupdated;

public class EventsSE {

    private String icon_url;
    private String event_name;
    private String status;
    private String id;
    private int team;


    public EventsSE() {
        // TODO Auto-generated constructor stub
    }

    public EventsSE(String id, String event_name,String status,String icon_url) {
        super();
        this.id = id;
        this.event_name = event_name;
        this.status = status;
        this.icon_url = icon_url;
        this.team = team;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }
}