package com.istevesitupdated;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.istevesit1.R;

import java.util.ArrayList;


public class Reg_view extends Fragment {
    View v;
    //RecyclerView variables
    RecyclerView recyclerView;
//    ArrayList<MainModels> mainModels;
//    MainAdapter mainAdapter;
    public  Reg_view(){
        //Empty constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        v = inflater.inflate(R.layout.activity_home,container,false);



        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()
//                .penaltyLog()
//                .penaltyDeath()
//                .build());
//        GetData call = new GetData();
//        String url = "http://istevesit.org/json/events_1.php";
//        try {
//            String response =call.get_data(url);
//            System.out.print("hi"+response);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        final String url = "http://istevesit.org/json/events_1.php";



        ArrayList<EventsSE> event_name = new ArrayList<EventsSE>();
//        listView = (ListView) view.findViewById(R.id.list);

//        new Events.AsyncHttpTask().execute(url);

        //recyclerview
        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
        //images
        Integer[] logo = {R.drawable.drawer,R.drawable.drawer,R.drawable.drawer,R.drawable.drawer,R.drawable.drawer,R.drawable.drawer,R.drawable.drawer,R.drawable.drawer};
        //text
        String[] name = {"Event 1","Event 2","Event 3","Event 4","Event 5","Event 6","Event 7","Event 8"};

//        mainModels = new ArrayList<>();
//        for(int i=0;i<logo.length;i++){
//            MainModels model = new MainModels(logo[i],name[i]);
//            mainModels.add(model);
//        }

        //layout
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext(),LinearLayoutManager.HORIZONTAL,false);
//        ConstraintLayout layoutManager = new ConstraintLayout(this.getContext(),ConstraintLayout.FOCUSABLES_TOUCH_MODE);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //adapter
//        mainAdapter = new MainAdapter(this.getContext(),mainModels);
//        recyclerView.setAdapter(mainAdapter);

    }

}
