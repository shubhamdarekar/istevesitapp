package com.istevesitupdated;

public class Winner_View {

    private String id;
    private String event_name;
    private String position1;
    private String name1;
    private String name2;
    private String name3;
    private String position2;
    private String name21;
    private String name22;
    private String name23;
    private String position3;
    private String name31;
    private String name32;
    private String name33;
    private String icon_url;

    public Winner_View() {
        // TODO Auto-generated constructor stub
    }

    public Winner_View(String name, String image) {
        super();
        this.id = id;
        this.event_name = event_name;
        this.position1 = position1;
        this.name1 = name1;
        this.name2 = name2;
        this.name3 = name3;
        this.position2 = position2;
        this.name21 = name21;
        this.name22 = name22;
        this.name23 = name23;
        this.position3 = position3;
        this.name31 = name31;
        this.name32 = name32;
        this.name33 = name33;
        this.icon_url = icon_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public void setPosition1(String position1) {
        this.position1 = position1;
    }

    public String getPosition2() {
        return position2;
    }

    public void setPosition2(String position2) {
        this.position2 = position2;
    }

    public String getName21() {
        return name21;
    }

    public void setName21(String name21) {
        this.name21 = name21;
    }

    public String getName22() {
        return name22;
    }

    public void setName22(String name22) {
        this.name22 = name22;
    }

    public String getName23() {
        return name23;
    }

    public void setName23(String name23) {
        this.name23 = name23;
    }

    public String getPosition3() {
        return position3;
    }

    public void setPosition3(String position3) {
        this.position3 = position3;
    }

    public String getName31() {
        return name31;
    }

    public void setName31(String name31) {
        this.name31 = name31;
    }

    public String getName32() {
        return name32;
    }

    public void setName32(String name32) {
        this.name32 = name32;
    }

    public String getName33() {
        return name33;
    }

    public void setName33(String name33) {
        this.name33 = name33;
    }

    public String getPosition1() {
        return position1;
    }

    public void setPosition(String position1) {
        this.position1 = position1;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

}