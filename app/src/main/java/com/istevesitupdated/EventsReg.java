package com.istevesitupdated;

public class EventsReg {

    private String image;
    private String name;
    private String post;

    public EventsReg() {
        // TODO Auto-generated constructor stub
    }

    public EventsReg(String name, String post, String image) {
        super();
        this.name = name;
        this.post = post;
        this.image = image;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}