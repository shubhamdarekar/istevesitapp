package com.istevesitupdated;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.istevesit1.R;


public class NavigationDrawerFragment extends Fragment {

    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;

    ListView list;
    String[] itemname ={
            "Home",
            "Register",
            "Events",
            "Council",
            "Winners",
            "Sponsors",
            "Contact Us",
            "About Us",

    };

    Integer[] imgid={
            R.mipmap.home1,
            R.mipmap.register,
            R.mipmap.googlecontroller1,
            R.mipmap.accountmultiple1,
            R.mipmap.trophyaward1,
            R.mipmap.bank1,
            R.mipmap.phone1,
            R.mipmap.information1
    };

    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private View mContainer;
    private boolean mDrawerOpened = false;
    Toolbar mToolBar;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = MyApplication.readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, false);
        mFromSavedInstanceState = savedInstanceState != null ? true : false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);


        NavigationDrawerListView adapter=new NavigationDrawerListView(getActivity(), itemname, imgid);
        list=(ListView) v.findViewById(R.id.list);
        list.setAdapter(adapter);
        fragmentManager = getFragmentManager();


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    switch( position )
                    {
                        case 0:
                            Reg_view rv = new Reg_view();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,rv);
                            fragmentTransaction.commit();
//                            getActivity ().finish ();
//                            Intent i=new Intent(getActivity(),MainActivity.class);
//                            startActivity(i);
                            break;
                        case 1:
                            registration_main Register = new registration_main ();
//                            Register Register = new Register();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,Register);
                            fragmentTransaction.commit();
                            break;


                        case 2:
                            Events Events = new Events ();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,Events);
                            fragmentTransaction.commit();
                            break;
                        case 3:
                            Council Council = new Council();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder, Council);
                            fragmentTransaction.commit();
                            break;
                        case 4:
                            Winner Winner = new Winner ();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder, Winner);
                            fragmentTransaction.commit();
                            break;
                        case 5:
                            Sponsorship Sponsorship = new Sponsorship();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,Sponsorship);
                            fragmentTransaction.commit();
                            break;
                        case 6:
                            ContactUs ContactUs= new ContactUs();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,ContactUs);
                            fragmentTransaction.commit();
                            break;
                        case 7:
                            AboutUs AboutUs = new AboutUs();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,AboutUs);
                            fragmentTransaction.commit();
                            break;

                       /* case 2:
                            Register Register = new Register();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Register);
                            fragmentTransaction.commit();
                            break;*/

                       /*case 0:
                            Intent i=new Intent(getActivity(),MainActivity.class);
                            startActivity(i);
                            break;

                        case 1:
                            NTU NTU = new NTU();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,NTU);
                            fragmentTransaction.commit();
                            break;

                        case 2:
                            Register Register = new Register();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Register);
                            fragmentTransaction.commit();
                            break;


                        case 3:
                            Events Events = new Events();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Events);
                            fragmentTransaction.commit();
                            break;
                        case 4:
                            Council Council = new Council();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder, Council);
                            fragmentTransaction.commit();
                            break;
                        case 5:
                            Winner Winner = new Winner();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder, Winner);
                            fragmentTransaction.commit();
                            break;
                        case 7:
                            Facebook Facebook = new Facebook();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder, Facebook);
                            fragmentTransaction.commit();
                            break;

                        case 8:
                            Instagram Instagram = new Instagram();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Instagram);
                            fragmentTransaction.commit();
                            break;
                        case 9:
                            Synergy Synergy = new Synergy();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Synergy);
                            fragmentTransaction.commit();
                            break;
                        case 10:
                            Sponsorship Sponsorship = new Sponsorship();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Sponsorship);
                            fragmentTransaction.commit();
                            break;
                        case 11:
                            ChairpersonsDesk ChairpersonsDesk = new ChairpersonsDesk();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,ChairpersonsDesk);
                            fragmentTransaction.commit();
                            break;
                        case 12:
                            ContactUs ContactUs= new ContactUs();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,ContactUs);
                            fragmentTransaction.commit();
                            break;
                        case 13:
                            AboutUs AboutUs = new AboutUs();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,AboutUs);
                            fragmentTransaction.commit();
                            break;
                        case 6:
                            Gallery Gallery = new Gallery();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Gallery);
                            fragmentTransaction.commit();
                            break;
                       /* case 2:
                            Register Register = new Register();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.fragmentholder,Register);
                            fragmentTransaction.commit();
                            break;*/


                    }
                mDrawerLayout.closeDrawer(mContainer);
                mToolBar.setTitle(itemname[position]);
            }
        });

        return v;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mContainer = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mToolBar = toolbar;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d("VIVZ", "onDrawerOpened");
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    MyApplication.saveToPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer);
                }
                getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d("VIVZ", "onDrawerClosed");
                getActivity().supportInvalidateOptionsMenu();
            }


        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
                if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
                    mDrawerLayout.openDrawer(mContainer);
                }
            }
        });


    }
}

