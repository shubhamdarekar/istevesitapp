package com.istevesitupdated;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.istevesit1.R;

public class ContactUs extends Fragment{
    private WebView mWebView;
    private Button site;

    public ContactUs() {
        // Required empty public constructor
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v=inflater.inflate(R.layout.activity_contact_us, container, false);
        mWebView = (WebView) v.findViewById(R.id.wbv);
        mWebView.loadUrl("https://www.google.co.in/maps/place/Vivekanand+Education+Society's+Institute+Of+Technology/@19.0462418,72.8881064,17.75z/data=!4m8!1m2!2m1!1svesit!3m4!1s0x0:0x40eb8beabd16cc2e!8m2!3d19.0453503!4d72.8890293?hl=en");

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient());
        site = (Button) v.findViewById(R.id.site) ;
        site.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new Site();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.contact, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                site.setVisibility(View.INVISIBLE);

            }        });


        return v;
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume ();
        getActivity().setTitle("Contact Us");
    }
}








