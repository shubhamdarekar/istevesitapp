package com.istevesitupdated;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.istevesit1.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Winner extends Fragment {

    private static final String TAG = "Http Connection";
    ArrayList<Winner_View> actorsList;

    Winner_Adapter adapter;
    private ProgressBar progressBar;

    private ListView listView;

    private ArrayAdapter arrayAdapter = null;

    private String[] blogTitles;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.winner1,container,false);



        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()

                .cacheInMemory(true)
                .cacheOnDisk(true)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())

                .defaultDisplayImageOptions(defaultOptions)

                .build();
        ImageLoader.getInstance().init(config); // Do it on Application start

        final String url = "http://istevesit.org/json/winners.php";



        actorsList = new ArrayList<Winner_View>();
        listView = (ListView) view.findViewById(R.id.list);

        new AsyncHttpTask().execute(url);

    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;

            HttpURLConnection urlConnection = null;

            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);

                urlConnection = (HttpURLConnection) url.openConnection();

                /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");

                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode ==  200) {

                    inputStream = new BufferedInputStream(urlConnection.getInputStream());

                    String response = convertInputStreamToString(inputStream);

                    parseResult(response);

                    result = 1; // Successful

                }else{
                    result = 0; //"Failed to fetch data!";
                }

            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }

            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPostExecute(Integer result) {
            pDialog.dismiss();
            /* Download complete. Lets update UI */
            if(result == 1){

                adapter = new Winner_Adapter(getContext(), R.layout.winner_custom_layout, actorsList);

                listView.setAdapter(adapter);

                listView.setOnItemClickListener ( new AdapterView.OnItemClickListener () {
                    @Override
                    public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {

                        PopupMenu popup =new PopupMenu ( getActivity (), view);
                        popup.getMenuInflater ().inflate ( R.menu.menu_popup,popup.getMenu () );

                        if(actorsList.get(position).getPosition1 ().equals ( "1" )){
                            popup.getMenu ().add (Menu.NONE,1,1, "First Position" );
                            if(actorsList.get(position).getPosition2 ().equals ( "1" )){
                                popup.getMenu ().add ( Menu.NONE,2,2, "Second Position" );
                                if(actorsList.get(position).getPosition3 ().equals ( "1" )){
                                    popup.getMenu ().add (Menu.NONE,3,3,  "Third Position" );
                                }
                            }
                        }





                        final View vv = view;
                        popup.setOnMenuItemClickListener ( new PopupMenu.OnMenuItemClickListener () {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                PopupWindow popup2 ;
                                if(item.getItemId () == 1){
                                    popup2 = popupDisplay(1,position);
                                    popup2.showAtLocation ( view,Gravity.CENTER_HORIZONTAL,0,0 );

                                }
                                else if(item.getItemId () == 2){
                                    popup2 = popupDisplay(2,position);
                                    popup2.showAtLocation ( view,Gravity.CENTER_HORIZONTAL,0,0 );
                                }
                                else if(item.getItemId () == 3){
                                    popup2 = popupDisplay(3,position);
                                    popup2.showAtLocation ( view,Gravity.CENTER_HORIZONTAL,0,0 );
                                }
                                return true;
                            }
                        } );



                        popup.show();

                    }
                    public PopupWindow popupDisplay( int myi,int position){ // disply designing your popoup window
                        final PopupWindow popupWindow = new PopupWindow(getActivity ());
                        LayoutInflater inflater = (LayoutInflater)   getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View v;
                        v = inflater.inflate(R.layout.winner_pop, null);

                        popupWindow.setFocusable(true);
                        popupWindow.setWidth( WindowManager.LayoutParams.WRAP_CONTENT);
                        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
                        popupWindow.setContentView(v);
                        switch (myi){
                            case 1:
                                if(!actorsList.get(position).getName1 ().equals ( "" )){
                                    TextView tv1 = (TextView)  v.findViewById ( R.id.team1 );
                                    tv1.setVisibility ( View.VISIBLE );
                                    tv1.setText ( actorsList.get(position).getName1 () );
                                }
                                if(!actorsList.get(position).getName2 ().equals ( "" )){
                                    TextView tv2 = (TextView)  v.findViewById ( R.id.team2 );
                                    tv2.setVisibility ( View.VISIBLE );
                                    tv2.setText ( actorsList.get(position).getName2 () );
                                }
                                if(!actorsList.get(position).getName3 ().equals ( "" )){
                                    TextView tv3 = (TextView)  v.findViewById ( R.id.team3 );
                                    tv3.setVisibility ( View.VISIBLE );
                                    tv3.setText ( actorsList.get(position).getName3 () );
                                }
                                break;
                            case 2:
                                if(!actorsList.get(position).getName21 ().equals ( "" )){
                                    TextView tv1 = (TextView)  v.findViewById ( R.id.team1 );
                                    tv1.setVisibility ( View.VISIBLE );
                                    tv1.setText ( actorsList.get(position).getName21 () );
                                }
                                if(!actorsList.get(position).getName22 ().equals ( "" )){
                                    TextView tv2 = (TextView)  v.findViewById ( R.id.team2 );
                                    tv2.setVisibility ( View.VISIBLE );
                                    tv2.setText ( actorsList.get(position).getName22 () );
                                }
                                if(!actorsList.get(position).getName23 ().equals ( "" )){
                                    TextView tv3 = (TextView)  v.findViewById ( R.id.team3 );
                                    tv3.setVisibility ( View.VISIBLE );
                                    tv3.setText ( actorsList.get(position).getName23 () );
                                }
                                v.findViewById ( R.id.team1 ).setVisibility ( View.VISIBLE );
                                break;
                            case 3:
                                if(!actorsList.get(position).getName31 ().equals ( "" )){
                                    TextView tv1 = (TextView)  v.findViewById ( R.id.team1 );
                                    tv1.setVisibility ( View.VISIBLE );
                                    tv1.setText ( actorsList.get(position).getName31 () );
                                }
                                if(!actorsList.get(position).getName32 ().equals ( "" )){
                                    TextView tv2 = (TextView)  v.findViewById ( R.id.team2 );
                                    tv2.setVisibility ( View.VISIBLE );
                                    tv2.setText ( actorsList.get(position).getName32 () );
                                }
                                if(!actorsList.get(position).getName33 ().equals ( "" )){
                                    TextView tv3 = (TextView)  v.findViewById ( R.id.team3 );
                                    tv3.setVisibility ( View.VISIBLE );
                                    tv3.setText ( actorsList.get(position).getName33 () );
                                }
                                break;
                        }


                        return popupWindow;
                    }
                } );

            }else{
                Log.e(TAG, "Failed to fetch data!");
            }
        }
    }


    private String convertInputStreamToString(InputStream inputStream) throws IOException {

        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));

        String line = "";
        String result = "";

        while((line = bufferedReader.readLine()) != null){
            result += line;
        }

        /* Close Stream */
        if(null!=inputStream){
            inputStream.close();
        }

        return result;
    }
    private void parseResult(String result) {

        try{
            JSONArray jarray = new JSONArray( result );

            for (int i = 0; i < jarray.length(); i++) {
                JSONObject object = jarray.getJSONObject(i);

                Winner_View actor = new Winner_View();
                actor.setId(object.getString("id"));
                actor.setEvent_name(object.getString("event_name"));
                actor.setPosition1(object.getString("position1"));
                actor.setName1(object.getString("name1"));
                actor.setName2(object.getString("name2"));
                actor.setName3(object.getString("name3"));
                actor.setPosition2(object.getString("position2"));
                actor.setName21(object.getString("name21"));
                actor.setName22(object.getString("name22"));
                actor.setName23(object.getString("name23"));
                actor.setPosition3(object.getString("position3"));
                actor.setName31(object.getString("name31"));
                actor.setName32(object.getString("name32"));
                actor.setName33(object.getString("name33"));
                actor.setIcon_url(object.getString("icon_url"));
                actorsList.add(actor);

            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    public class Winner_Adapter extends ArrayAdapter<Winner_View> {
        ArrayList<Winner_View> actorList;
        LayoutInflater vi;
        int Resource;
        ViewHolder holder;

        public Winner_Adapter(Context context, int resource, ArrayList<Winner_View> objects) {
            super(context, resource, objects);
            vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resource = resource;
            actorList = objects;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // convert view = design
            View v = convertView;
            if (v == null) {
                holder = new ViewHolder();
                v = vi.inflate(Resource, null);
                holder.imageview = (ImageView) v.findViewById(R.id.thumbnail);
                holder.imageview.setImageDrawable ( null );
                holder.title = (TextView) v.findViewById(R.id.title1);


                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            final ProgressBar progressBar=(ProgressBar)v.findViewById(R.id.progress);

            ImageLoader.getInstance().displayImage(actorList.get(position).getIcon_url().replace("\\","/"), holder.imageview, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);
                }
            }); // Default options will be used
            holder.title.setText(actorList.get(position).getEvent_name());


            return v;

        }

        class ViewHolder {
            public ImageView imageview;
            public TextView title;


        }
    }

    @Override
    public void onResume() {
        super.onResume ();
        getActivity().setTitle("Winners");
    }

}