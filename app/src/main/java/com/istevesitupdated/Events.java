package com.istevesitupdated;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.istevesit1.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Events extends Fragment {

    private static final String TAG = "Http Connection";
    ArrayList<EventsSE> actorsList;

    SEEvent_Adapter adapter;

    private ListView listView;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.events1,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()

                .cacheInMemory(true)
                .cacheOnDisk(true)

                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())

                .defaultDisplayImageOptions(defaultOptions)

                .build();
        ImageLoader.getInstance().init(config); // Do it on Application start

        final String url = "http://istevesit.org/json/events.php";



        actorsList = new ArrayList<EventsSE>();
        listView = (ListView) view.findViewById(R.id.list);

        new AsyncHttpTask().execute(url);

    }

    public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;

            HttpURLConnection urlConnection = null;

            Integer result = 0;
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);

                urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");

                int statusCode = urlConnection.getResponseCode();

                /* 200 represents HTTP OK */
                if (statusCode ==  200) {

                    inputStream = new BufferedInputStream(urlConnection.getInputStream());

                    String response = convertInputStreamToString(inputStream);

                    parseResult(response);

                    result = 1; // Successful

                }else{
                    result = 0; //"Failed to fetch data!";
                }

            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }

            return result; //"Failed to fetch data!";
        }


        @Override
        protected void onPostExecute(Integer result) {
            pDialog.dismiss();
            /* Download complete. Lets update UI */
            if(result == 1){

                adapter = new SEEvent_Adapter(getContext(), R.layout.events1_row, actorsList);

                listView.setAdapter(adapter);
                listView.setOnItemClickListener ( new AdapterView.OnItemClickListener () {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(actorsList.get ( position ).getStatus ().equals ( "1" )){
                            Register Register = new Register();
                            Bundle args = new Bundle (  );
                            args.putInt ( "team",actorsList.get ( position ).getTeam () );
                            args.putString ( "event",actorsList.get(position).getEvent_name () );
                            args.putString ( "eventid",actorsList.get ( position ).getId () );
                            Register.setArguments ( args );
                            fragmentManager = getFragmentManager();
                            fragmentTransaction = fragmentManager.beginTransaction().addToBackStack ( null );
                            fragmentTransaction.replace(R.id.fragmentholder,Register);
                            getActivity().setTitle("Register");
                            fragmentTransaction.commit();
                        }

                    }
                } );

            }else{
                Log.e(TAG, "Failed to fetch data!");
            }
        }
    }


    private String convertInputStreamToString(InputStream inputStream) throws IOException {

        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));

        String line = "";
        String result = "";

        while((line = bufferedReader.readLine()) != null){
            result += line;
        }

            /* Close Stream */
        if(null!=inputStream){
            inputStream.close();
        }

        return result;
    }
    private void parseResult(String result) {

        try{
            JSONArray jarray = new JSONArray ( result );

            for (int i = 0; i < jarray.length(); i++) {
                JSONObject object = jarray.getJSONObject(i);

                EventsSE actor = new EventsSE();
                actor.setEvent_name ( object.getString("event_name") );

                actor.setIcon_url (object.getString("icon_url"));
                actor.setId (object.getString("id"));
                actor.setStatus (object.getString("status"));

                actorsList.add(actor);

            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }
    public class SEEvent_Adapter extends ArrayAdapter<EventsSE> {
        ArrayList<EventsSE> actorList;
        LayoutInflater vi;
        int Resource;
        ViewHolder holder;

        public SEEvent_Adapter(Context context, int resource, ArrayList<EventsSE> objects) {
            super(context, resource, objects);
            vi = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resource = resource;
            actorList = objects;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // convert view = design
            View v = convertView;
            if (v == null) {
                holder = new ViewHolder();
                v = vi.inflate(Resource, null);
                holder.imageview = (ImageView) v.findViewById(R.id.thumbnail);
                holder.imageview.setImageDrawable ( null );
                holder.title = (TextView) v.findViewById(R.id.title1);
                holder.button = (TextView) v.findViewById ( R.id.button2 );


                v.setTag(holder);
            } else {
                holder = (ViewHolder) v.getTag();
            }

            final ProgressBar progressBar=(ProgressBar)v.findViewById(R.id.progress);
            ImageLoader imgld = ImageLoader.getInstance ();
            imgld.displayImage(actorList.get(position).getIcon_url ().replace ( "\\", "/" ), holder.imageview, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    progressBar.setVisibility(View.GONE);
                }
            });
            Bitmap bmp = imgld.loadImageSync(actorList.get(position).getIcon_url ());
            // Default options will be used
            holder.title.setText(actorList.get(position).getEvent_name ());
            if (actorList.get ( position ).getStatus ().equals ( "1" )) {
                holder.title.setBackgroundColor (getResources().getColor(R.color.DarkCyan));
                holder.button.setVisibility ( View.VISIBLE );
            }else if(actorList.get ( position ).getStatus ().equals ( "0" )){
                holder.title.setBackgroundColor (getResources().getColor(R.color.DarkCyan));
                holder.button.setVisibility ( View.GONE );
            }
            else if(actorList.get ( position ).getStatus ().equals ( "2" )){
                holder.title.setBackgroundColor (Color.parseColor("#555555") );
                holder.button.setVisibility ( View.GONE );
            }

            return v;

        }

        class ViewHolder {
            public ImageView imageview;
            public TextView title;
            public TextView button;



        }

        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
            ImageView bmImage;

            public DownloadImageTask(ImageView bmImage) {
                this.bmImage = bmImage;
            }

            protected Bitmap doInBackground(String... urls) {
                String urldisplay = urls[0];
                Bitmap mIcon11 = null;
                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    e.printStackTrace();
                }
                return mIcon11;
            }

            protected void onPostExecute(Bitmap result) {
                bmImage.setImageBitmap(result);
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume ();
        getActivity().setTitle("Events");
    }
}