package com.istevesitupdated.Home_rv;

public class MainModels {
    private Integer logo;
    private String name;

    public MainModels(Integer logo,String name){
        this.logo = logo;
        this.name = name;
    }


    public Integer getLogo(){
        return logo;
    }

    public String getEventName(){
        return name;
    }
}
